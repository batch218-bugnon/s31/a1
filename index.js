let http = require("http");

let port = 3000;

let server = http.createServer(function (reqs, res){

	if (reqs.url == "/login"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Welcome to the login page.");
	}
	else  {
		res.writeHead(404, {"Content-Type" : "text/plain"});
		res.end("I'm sorry the page you are looking for cannot be found.")	
	}
});

server.listen(port);
console.log(`Server is now available at localhost: ${port}`);
	
